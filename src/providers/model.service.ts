import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HTTP, HTTPResponse } from '@ionic-native/http';
//import { Observable } from "rxjs/Observable";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/timeout';

import { AlertController, Platform, ToastController } from "ionic-angular";
import { Network } from '@ionic-native/network';
declare var navigator: any;

/*
  Generated class for the Model provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ModelService {

  public SiteUrl = "https://www.ngmobileapp.com.ng/cod";
  public Api = "https://www.ngmobileapp.com.ng/cod/api";
  timeOut: number = 15000;
  //public SiteUrl: string = "http://localhost:8888/church-server";
  //public Api: string = "http://localhost:8888/church-server/api";
  EventData: any;
  //public Network_Status: boolean;

  constructor(
    public http: Http,
    public _http: HTTP,
    private alertCtrl: AlertController,
    private platform: Platform,
    private network: Network,
    public toastCtrl: ToastController
  ) {
    platform.ready().then(() => {
      //this.Network_Status = navigator.onLine;
    });
  }

  public network_status(): boolean {
    let status = false;
    this.platform.ready().then(() => {
      status = navigator.onLine;
    });
    return status;
  }

  public disconnect_error() {
    return this.platform.ready().then(() => {
      if (navigator.onLine === false) {
        this.dfAlert({
          title: "Connection Error",
          subTitle: "You are not connected to the internet, " +
            "please connect and retry\nCode: ERR_NET!"
        });
        return false;
      }
    });
  }


  public handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(error, 'status error');
    return Observable.throw(error);
  }


  showToast(msg: any, position: string = "bottom") {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: position
    });
    toast.present(toast);
  }

  pledges() {
    let url: string = this.Api + "/pledges";
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  pledge_details(pledge_id, member_id) {
    let url: string = this.Api + "/pledge-details/" + pledge_id + "/" + member_id;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }

  make_pledge(member_id, pledge_id, amount) {
    let url: string = this.Api + "/make-a-pledge/" + member_id + "/" + pledge_id + "/" + amount;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }

  /*
  make_pledge(data: any) {
    let url: string = this.Api + "/make-a-pledge";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  */

  my_pledges(member_id) {
    let url: string = this.Api + "/member-pledges/" + member_id;
    // console.log(url);
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  pledge_summary(member_id, amount) {
    let url: string = this.Api + "/pledge-summary/" + +member_id + "/" + amount;

    // console.log(url);
    return this.http.get(url)
      .timeout(this.timeOut)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  /*
  pledge_summary(data: any) {
    let url: string = this.Api + "/pledge-summary";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }
  $member_id, $pledge_id, $amount, $commission, $ref
  */

  pledge_done(member_id, pledge_id, members_pledges_id, amount, commission, ref) {
    let url: string = this.Api + "/pledge-done/" + member_id + "/" + pledge_id + "/" + members_pledges_id + "/" + amount + "/" + commission + "/" + ref;
    // console.log(url);
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }
  /*
    pledge_done(data: any) {
      let url: string = this.Api + "/pledge-done";
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      let options = new RequestOptions({ headers: headers });
      return this.http.post(url, JSON.stringify(data), options)
        .map(res => res.json())
        .do(data => console.log("All: " + JSON.stringify(data)))
        .catch(this.handleError);
    }
  */

  my_pledge_details(members_pledges_id) {
    let url: string = this.Api + "/member-pledge-details/" + members_pledges_id;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  daily_passage() {
    // https://beta.ourmanna.com/api/v1/get/?format=json
    //let url: string = 'https://beta.ourmanna.com/api/v1/get/?format=json';
    let url: string = this.Api + "/daily-passage";    
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      // .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  bulletin() {
    let url: string = this.Api + "/bulletin";
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  podcast() {
    let url: string = this.Api + "/podcast";
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  gallery() {
    let url: string = this.Api + "/gallery";
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  gallery_details(ID) {
    let url: string = this.Api + "/gallery-details/" + ID;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  login(username, password) {
    let url: string = this.Api + "/login/" + username + "/" + password;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }

  register(email, username, password, first_name, last_name) {
    let url: string = this.Api + "/register/" + email + "/" + username + "/" + password + "/" + first_name + "/" + last_name;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }

  edit_account(data: any) {
    let url: string = this.Api + "/edit-account";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  forgot_password(data: any) {
    let url: string = this.Api + "/edit-account";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }







  donations_summary(member_id, amount) {
    let url: string = this.Api + "/donations-summary/" + member_id + "/" + amount;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }


  donations_done(member_id, ref, amount, commission, purpose) {
    let url: string = this.Api + "/donations-done/" + member_id + "/" + ref
      + "/" + amount + "/" + commission + "/" + purpose;
    return this.http.get(url)
    .timeout(this.timeOut)
      .map(res => res.json())
      .catch(this.handleError);
  }


  dfAlert(data: any, setFocus = null) {
    let alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.subTitle,
      buttons: [{
        text: 'Ok', handler: (data) => {
          if (setFocus) {
            setFocus.setFocus();
          }
        }
      }]
    });
    alert.present();
  }

  auth(): boolean {
    let logged = window.localStorage.getItem('Logged_In');
    if (logged === "true") {
      return true
    }
    return false;
  }

  loginSet(data) {
    window.localStorage.setItem("Logged_In", "true");
    window.localStorage.setItem("Member_ID", data.Token);
    window.localStorage.setItem("Username", data.Username);
    window.localStorage.setItem("FirstName", data.FirstName);
    window.localStorage.setItem("LastName", data.LastName);
    window.localStorage.setItem("Email", data.Email);
    window.localStorage.setItem("DisplayName", data.DisplayName);
  }

  logoutSet() {
    window.localStorage.removeItem("Logged_In");
    window.localStorage.removeItem('Member_ID');
    window.localStorage.removeItem("Username");
    window.localStorage.removeItem("FirstName");
    window.localStorage.removeItem("LastName");
    window.localStorage.removeItem("Email");
    window.localStorage.removeItem("DisplayName");
  }

  checkValue(value: any): boolean {
    if (value == null || value == "" || value == undefined || value == "undefined") {
      return false;
    }
    return true;
  }


  autoToggleToolbar(ionScroll, showheader, hideheader, myElement) {
    let start = 0;
    let threshold = 50;

    ionScroll = myElement.getElementsByClassName('scroll-content')[0];
    ionScroll.addEventListener("scroll", () => {
      if (ionScroll.scrollTop - start > threshold) {
        showheader = true;
        hideheader = false;
      } else {
        showheader = false;
        hideheader = true;
      }
    });
  }

  substring(value: any, start: number, end: number): any {
    return value.substring(start, end);
  }



}
