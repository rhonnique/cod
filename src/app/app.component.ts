import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, LoadingController, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PodcastPage } from '../pages/podcast/podcast';
import { BiblePassagePage } from '../pages/bible-passage/bible-passage';
import { SermonsPage } from '../pages/sermons/sermons';
import { BulletinsPage } from '../pages/bulletins/bulletins';
import { PledgesPage } from '../pages/pledges/pledges';
import { DonationsPage } from '../pages/donations/donations';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
// import { ModelService } from '../providers/model.service';
import { MyPledgesPage } from '../pages/my-pledges/my-pledges';
import { AboutPage } from '../pages/about/about';
import { EditAccountPage } from '../pages/edit-account/edit-account';
import { LiveStreamingPage } from '../pages/live-streaming/live-streaming';
import { GalleryPage } from '../pages/gallery/gallery';
import { ModelService } from '../providers/model.service';
import { LiveRadioPage } from '../pages/live-radio/live-radio';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  confirmAlert: any;
  //public alertShown: boolean = false;
  showedAlert: boolean;
  pageLogin: any;
  pageRegister: any;
  pageMyPledges: any;
  pageEditAccount: typeof EditAccountPage;

  user_email: any;
  user_name: any;
  auth: boolean;
  // auth_page: any;

  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{ icon: string, title: string, component: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public appCtrl: App,
    public loadingCtrl: LoadingController,
    public events: Events,
    public alertCtrl: AlertController,
    public modelService: ModelService
  ) {
    this.initializeApp();

    this.pageLogin = LoginPage;
    this.pageRegister = RegisterPage;
    this.pageMyPledges = MyPledgesPage;
    this.pageEditAccount = EditAccountPage;

    this.auth = modelService.auth();
    if (modelService.auth() === true) {
      this.set_login();
    }

    // used for an example of ngFor and navigation
    this.pages = [
      // { icon: 'home', title: 'Home', component: HomePage },
      { icon: 'headset', title: 'Podcast', component: PodcastPage },
      { icon: 'logo-youtube', title: 'Sermons', component: SermonsPage },
      { icon: 'book', title: 'Daily Bible Passage', component: BiblePassagePage },
      { icon: 'list-box', title: 'Bulletins', component: BulletinsPage },
      { icon: 'heart', title: 'E-Giving', component: DonationsPage },
      { icon: 'hand', title: 'Make A Pledge', component: PledgesPage },
      { icon: 'desktop', title: 'Live Streaming', component: LiveStreamingPage },
      { icon: 'radio', title: 'Live Radio', component: LiveRadioPage },
      { icon: 'images', title: 'Photo Gallery', component: GalleryPage },
      { icon: 'information-circle', title: 'About The Church', component: AboutPage },
    ];

    events.subscribe('user:login', (userData) => {
      this.set_login();
    });

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.showedAlert = false;

      // Confirm exit
      this.platform.registerBackButtonAction(() => {
        if (this.nav.length() == 1) {
          if (!this.showedAlert) {
            this.confirmExitApp();
          } else {
            this.showedAlert = false;
            this.confirmAlert.dismiss();
          }
        }

        this.nav.pop();
      });

    });
  }

  


  confirmExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
      title: 'Confirm Exit',
      message: 'Do you want Exit?',
        buttons: [
            {
                text: 'Cancel',
                handler: () => {
                    this.showedAlert = false;
                    return;
                }
            },
            {
                text: 'Yes',
                handler: () => {
                    this.platform.exitApp();
                }
            }
        ]
    });
    this.confirmAlert.present();
}

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // this.nav.setRoot(page.component);
    this.nav.push(page.component);
  }

  loadPage(page) {
    this.nav.push(page);
  }

  pushPage(page) {
    this.nav.push(page);
  }

  set_login() {
    this.user_name = window.localStorage.getItem("DisplayName");
    this.user_email = window.localStorage.getItem("Email");
    this.auth = true;
  }


  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    setTimeout(() => {
      this.modelService.logoutSet();
      this.auth = false;
      loading.dismiss();
      this.appCtrl.getRootNav().setRoot(HomePage);
    }, 1000);
  }

  

}
