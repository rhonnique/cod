import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HTTP } from '@ionic-native/http';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Network } from '@ionic-native/network';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { YtProvider } from '../providers/yt/yt';
import { ModelService } from '../providers/model.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';



import { HomePage } from '../pages/home/home';
import { AboutPageModule } from '../pages/about/about.module';
import { AboutServicesPageModule } from '../pages/about-services/about-services.module';
import { AboutStatementPageModule } from '../pages/about-statement/about-statement.module';
import { AboutContactPageModule } from '../pages/about-contact/about-contact.module';
import { AboutVisionPageModule } from '../pages/about-vision/about-vision.module';
import { PodcastPageModule } from '../pages/podcast/podcast.module';
import { BiblePassagePageModule } from '../pages/bible-passage/bible-passage.module';
import { SermonsPageModule } from '../pages/sermons/sermons.module';
import { BulletinsPageModule } from '../pages/bulletins/bulletins.module';
import { PledgesPageModule } from '../pages/pledges/pledges.module';
import { DonationsPageModule } from '../pages/donations/donations.module';
import { PledgeDetailsPageModule } from '../pages/pledge-details/pledge-details.module';
import { LoginPageModule } from '../pages/login/login.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { MyPledgesPageModule } from '../pages/my-pledges/my-pledges.module';
import { MyPledgeDetailsPageModule } from '../pages/my-pledge-details/my-pledge-details.module';
import { EditAccountPageModule } from '../pages/edit-account/edit-account.module';
import { LiveStreamingPageModule } from '../pages/live-streaming/live-streaming.module';
import { GalleryPageModule } from '../pages/gallery/gallery.module';
import { GalleryDetailsPageModule } from '../pages/gallery-details/gallery-details.module';
import { LiveRadioPageModule } from '../pages/live-radio/live-radio.module';
import { ForgotPasswordPageModule } from '../pages/forgot-password/forgot-password.module';






@NgModule({
  declarations: [
    MyApp,
    HomePage
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AboutPageModule,
    AboutServicesPageModule,
    AboutStatementPageModule,
    AboutContactPageModule,
    AboutVisionPageModule,
    PodcastPageModule,
    BiblePassagePageModule,
    SermonsPageModule,
    BulletinsPageModule,
    PledgesPageModule,
    DonationsPageModule,
    PledgeDetailsPageModule,
    LoginPageModule,
    RegisterPageModule,
    ForgotPasswordPageModule,
    MyPledgesPageModule,
    MyPledgeDetailsPageModule,
    EditAccountPageModule,
    LiveStreamingPageModule,
    LiveRadioPageModule,
    GalleryPageModule,
    GalleryDetailsPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ModelService,
    YoutubeVideoPlayer,
    YtProvider,
    InAppBrowser,
    HTTP
  ]
})
export class AppModule {}
