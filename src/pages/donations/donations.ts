import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SlicePipe } from '@angular/common';
import { ModelService } from '../../providers/model.service';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import * as $ from 'jquery';
import { HomePage } from '../home/home';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
declare var PaystackPop: any;

/**
 * Generated class for the DonationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-donations',
  templateUrl: 'donations.html',
})
export class DonationsPage {

  member_id: any;
  nameInterval: number;
  email: any;
  ios: boolean;
  subAccount: any;
  //@ViewChild('donationBtn') donationBtn;
  pushHome: any;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "E-Giving";
  /** Toggle Toolbar Ends */


  transactionStatusMsg: any;
  transResponse: any;
  transactionStatus: any;
  date: any;
  //donation_amount: any;
  //@ViewChild('donation_amount') donation_amount: ElementRef;
  donationForm: FormGroup;
  auth: boolean;
  amount: any;
  type: any;
  type_others: any;
  purpose: any;
  showTypeOthers: boolean = false;
  donationBtn: boolean = false;

  viewDonate: boolean = true;
  viewSummary: boolean = false;
  viewDone: boolean = false;

  paystackKey: string;
  ref: string;
  postAmount: any;
  commission: any;
  total: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public modelService: ModelService,
    public loadingCtrl: LoadingController,
    private elementRef: ElementRef,
    private platform: Platform,
    private inAppBrowser: InAppBrowser
  ) {
    this.pushHome = HomePage;

    this.auth = modelService.auth();
    this.donationForm = this.fb.group({
      amount: [null],
      type: [null],
      type_others: [null]
    });

    this.amount = navParams.get('donation_amount');
    this.type = navParams.get('donation_type');
    this.type_others = navParams.get('donation_type_others');

  }

  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */

    if (this.platform.is('ios')) {
      this.ios = true;
    }

    this.email = window.localStorage.getItem("Email");
    this.member_id = window.localStorage.getItem("Member_ID");

  }

  ionViewDidLoad() {
    var ps = document.createElement("script");
    ps.type = "text/javascript";
    ps.src = "https://js.paystack.co/v1/inline.js";
    this.elementRef.nativeElement.appendChild(ps);
  }

  pushLogin() {
    this.navCtrl.push(LoginPage, {
      return: 'DonationsPage',
      donation_amount: this.amount,
      donation_type: this.type,
      donation_type_others: this.type_others
    });
  }

  pushRegister() {
    this.navCtrl.push(RegisterPage, {
      return: 'DonationsPage',
      donation_amount: this.amount,
      donation_type: this.type,
      donation_type_others: this.type_others
    });
  }

  setTypeOption(value) {
    if (value == "Others") {
      this.showTypeOthers = true;
    } else {
      this.showTypeOthers = false;
    }
  }

  summary(post: any) {
    if (!this.modelService.checkValue(post.amount)) {
      this.modelService.dfAlert({
        title: "Validation Error",
        subTitle: 'Amount is required!'
      });
      return;
    }

    if (!this.modelService.checkValue(post.type)) {
      this.modelService.dfAlert({
        title: "Validation Error",
        subTitle: 'Donation type is required!'
      });
      return;
    }

    this.purpose = post.type;

    if (post.type == 'Others') {
      if (!this.modelService.checkValue(post.type_others)) {
        this.modelService.dfAlert({
          title: "Validation Error",
          subTitle: 'Other type is required!'
        });
        return;
      }

      this.purpose = post.type_others;
    }

    let data = {};
    data['member_id'] = window.localStorage.getItem("Member_ID");
    data['amount'] = post.amount;

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.donations_summary(data['member_id'], data['amount']).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {

          this.ref = data.details.Ref;
          this.paystackKey = data.details.Paystack_Key;
          this.subAccount = data.details.Sub_Account;
          this.commission = data.details.Commission;
          this.postAmount = data.details.Post_Amount;
          this.total = data.details.Total;

          this.viewDonate = false;
          this.viewSummary = true;
          this.viewDone = false;
        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_001'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_002'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      });
  }


  payStack() {
    if (this.ios === true) {
      this.modelService.showToast("Redirecting to payment page, please wait...");

      const options: InAppBrowserOptions = {
        location: 'no',
        zoom: 'yes'
      }

      let url = this.modelService.SiteUrl + '/make-payment?type=donation&key=' + this.paystackKey + '&email=' + this.email +
        '&amount=' + this.amount + '&ref=' + this.ref + '&member_id=' + this.member_id + '&commission=' + this.commission
        + '&purpose=' + encodeURIComponent(this.purpose); 


      console.log(url);
      const browser = this.inAppBrowser.create(url, '_system', options);


      browser.on("loadstop").subscribe(event => {
        browser.executeScript({ code: "localStorage.setItem('close', '')" });
        this.nameInterval = setInterval(function () {
          
          browser.executeScript({ code: "localStorage.getItem('close')" })
            .then((values) => {
              if(values[0] == 'true'){
                browser.close;
              }
            });

        }, 100)
      });


      browser.on("exit").subscribe(() => {
        clearInterval(this.nameInterval);
      });
    } else {
      this.modelService.showToast("Loading payment window, please wait...");

      try {
        let ref = this.ref
        var handler = PaystackPop.setup({
          key: this.paystackKey,
          email: this.email,
          amount: this.postAmount,
          //subaccount: this.subAccount,
          ref: ref,
          // meta:[{ flightid:3849 }],
          callback: (response) => {
            // console.log('success. transaction ref is ' + response.reference);
            this.completeTransaction(response.reference);
          },
          onClose: function () {
            console.log('window closed');
          }
        });
        handler.openIframe();
      } catch (e) {
        // No content response..
        console.log(e);
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      }

    }

  }


  completeTransaction(ref) {
    let data = {};
    data['member_id'] = this.member_id;
    data['ref'] = ref;
    data['amount'] = this.amount;
    data['commission'] = this.commission;
    data['purpose'] = this.purpose;
    /**/
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.donations_done(data['member_id'], data['ref'], data['amount'],
      data['commission'], data['purpose']).subscribe(data => {
        loading.dismiss();
        if (data) {
          if (data.response == "Success") {
            console.log(data.details);
            this.transResponse = data.details;
            this.ref = ref;
            this.commission = this.commission;
            this.total = data.details.Total;
            this.date = data.details.Date;
            this.transactionStatusMsg = data.details.Status_Msg;

            this.viewDonate = false;
            this.viewSummary = false;
            this.viewDone = true;
          } else {

          }
        } else {

        }
      },
        error => {
          //loading.dismiss();
        });
  }

  _viewDonate() {
    this.viewDonate = true;
    this.viewSummary = false;
    this.viewDone = false;
  }

  _reset() {
    this.viewDonate = true;
    this.viewSummary = false;
    this.viewDone = false;
    this.purpose = null;
    this.transactionStatusMsg = null;
    this.transResponse = null;
    this.transactionStatus = null;
    this.date = null;
    this.amount = null;
    this.type = null;
    this.type_others = null;
    this.showTypeOthers = null;
    this.paystackKey = null;
    this.subAccount = null;
    this.ref = null;
    this.postAmount = null;
    this.commission = null;
    this.total = null;
  }

}
