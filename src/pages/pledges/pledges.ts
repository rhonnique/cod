import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { PledgeDetailsPage } from '../pledge-details/pledge-details';

/**
 * Generated class for the PledgesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pledges',
  templateUrl: 'pledges.html',
})
export class PledgesPage {
  pledgeDetails: any;
  pledges: any;
  site_url: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modelService: ModelService) {
      this.site_url = modelService.SiteUrl;
  }

  ionViewDidLoad() {
    this.getPledges();
  }


  getPledges(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.pledges().subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          this.pledges = data.details;
        } else {
          
        }
      } else {

      }
    },
      error => {
        loading.dismiss();
      });
  }

  pushDetails(pledge){
    this.navCtrl.push(PledgeDetailsPage, {
      pledge_id: pledge.pledge_id,
      title: pledge.name
    });
  }


}
