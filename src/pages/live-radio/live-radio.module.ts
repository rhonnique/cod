import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveRadioPage } from './live-radio';

@NgModule({
  declarations: [
    LiveRadioPage,
  ],
  imports: [
    IonicPageModule.forChild(LiveRadioPage),
  ],
})
export class LiveRadioPageModule {}
