import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutVisionPage } from './about-vision';

@NgModule({
  declarations: [
    AboutVisionPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutVisionPage),
  ],
})
export class AboutVisionPageModule {}
