import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform } from 'ionic-angular';
import { AboutServicesPage } from '../about-services/about-services';
import { AboutStatementPage } from '../about-statement/about-statement';
import { AboutContactPage } from '../about-contact/about-contact';
import { AboutVisionPage } from '../about-vision/about-vision';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as $ from 'jquery';
declare var cordova: any;
import { ModelService } from '../../providers/model.service';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  pageService = AboutServicesPage;
  pageStatement = AboutStatementPage;
  pageContact = AboutContactPage;
  pageVision = AboutVisionPage;
  link_facebook = 'https://www.facebook.com/rccgcod';
  link_twitter = 'https://twitter.com/RCCGCityofDavid';
  link_instagram = 'https://www.instagram.com/rccgcityofdavid/?hl=en';
  tab: number = 0;
  tab_open: boolean;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "About The Church";
  /** Toggle Toolbar Ends */

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private elementRef: ElementRef,
    public modalCtrl: ModalController,
    private inAppBrowser: InAppBrowser,
    private modelService: ModelService,
    private platform: Platform) {

  }


  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AboutPage');
  }

  openModal(Page) {
    let modal = this.modalCtrl.create(Page);
    modal.present();
  }


  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: 'no'
    }

    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, '_self', options);

    // Inject scripts, css and more with browser.X
  }

  /*
  if(tab_body.is(':visible')){
      tab_body.slideUp('fast');
      //$(this).find( "ion-icon" ).css( "color", "black" );
      //$('.item-heading-' + id + ' ion-icon[name^="arrow-"]').hide();
      //$('.item-heading-' + id + ' ion-icon').attr('name','arrow-back');
      // $('.item-heading-' + id + ' ion-icon[name="arrow-down"]').hide();
    } else {
      $('.item-body').slideUp('fast');
      tab_body.slideDown('fast');
      
      //$('.item-heading-' + id + ' ion-icon[name^="arrow-"]').show();
      //$('.item-heading-' + id + ' ion-icon').attr('name','arrow-down');
      // $('.item-heading-' + id + ' ion-icon[name="arrow-back"]').hide();
      // $('.item-heading-' + id + ' ion-icon[name="arrow-down"]').show();
    }
    */


  toggle_tab(id) {
    this.tab = Number(id);
    console.error(this.tab);
    let tab_body = $('.item-body-' + id);
    if (tab_body.is(':visible')) {
      tab_body.slideUp('fast');
      this.tab_open = false;
      //$('.item-heading-' + id + ' ion-icon[name^="arrow-"]').prop('name','arrow-back');
    } else {
      $('.item-body').slideUp('fast');
      tab_body.slideDown('fast');
      this.tab_open = true;
      //$('.item-heading-' + id + ' ion-icon[name^="arrow-"]').prop('name','arrow-down');
    }

  }



  open_page() {
    //var ref = cordova.InAppBrowser.open('http://apache.org', '_blank', 'location=yes');
    //var win = window.open( "http://google.com", "_blank", "location=yes" );

    var name;
    var nameInterval;

    const options: InAppBrowserOptions = {
      location: 'yes',
      zoom: 'no'
    }

    const win = this.inAppBrowser.create(this.modelService.SiteUrl + '/make-payment', '_self', options);

    win.on('loadstart').subscribe(event => {
      //browser.insertCSS({ code: "body{color: red;" });
      console.log('loadstart');
      alert('loadstart');
   });



    /*


    this.platform.ready().then( () => {
      const browser = cordova.InAppBrowser.open('abc.com', '_blank', 'location=no,toolbar=no');
      browser.addEventListener('loadstart', (event) => {
        console.log('loadstart');
      });
    });

    win.addEventListener('loadstop').subscribe(function () {
      win.executeScript({ code: "localStorage.setItem('name', '')" });
      nameInterval = setInterval(function () {
        win.executeScript({ code: "localStorage.getItem('name')" })
          .then((values) => {
            //console.log(values);
            name = values[0];
            document.getElementById('output').innerText = name;
            win.close();
          });
      }, 100)
    });

    win.addEventListener('exit').subscribe(function () {
      clearInterval(nameInterval);
      document.getElementById('output').innerText = name;
      win.close();
    });


    





    win.addEventListener("loadstop", function () {
      win.executeScript({ code: "localStorage.setItem('name', '')" });
      nameInterval = setInterval(function () {
        win.executeScript({ code: "localStorage.getItem('name')" }, function (values) {
          name = values[0];
        });
      }, 100)
    });

    
    win.addEventListener("loadstop", function () {
      win.executeScript({ code: "localStorage.setItem('name', '')" });
      nameInterval = setInterval(function () {
        win.executeScript({ code: "localStorage.getItem('name')" }, 
        function (values) {
          name = values[0];
        });
      }, 100)
    });

    win.addEventListener('exit', function () {
      clearInterval(nameInterval);
      document.getElementById('output').innerText = name;
    });
    */

  }



}
