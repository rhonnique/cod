import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutStatementPage } from './about-statement';

@NgModule({
  declarations: [
    AboutStatementPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutStatementPage),
  ],
})
export class AboutStatementPageModule {}
