import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PledgeDetailsPage } from './pledge-details';

@NgModule({
  declarations: [
    PledgeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PledgeDetailsPage),
  ],
})
export class PledgeDetailsPageModule {}
