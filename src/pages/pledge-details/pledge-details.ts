import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MyPledgeDetailsPage } from '../my-pledge-details/my-pledge-details';

/**
 * Generated class for the PledgeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pledge-details',
  templateUrl: 'pledge-details.html',
})
export class PledgeDetailsPage {
  members_pledges_id: any;
  site_url: string;
  pledged_amount: any;
  viewMyPledge: boolean;
  myPledge: any;
  pledgeForm: FormGroup;
  auth: boolean;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "Donations";
  /** Toggle Toolbar Ends */

  pledgeDetails: any;
  pledgeTitle: any;
  pledgeID: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modelService: ModelService,
    public loadingCtrl: LoadingController,
    public fb: FormBuilder,
    private elementRef: ElementRef) {
    this.site_url = modelService.SiteUrl;

    this.auth = modelService.auth();
    this.pledgeID = navParams.get('pledge_id');

    this.pledgeForm = fb.group({
      amount: [null, Validators.required]
    });
  }

  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */

  }

  ionViewDidLoad() {
    //console.log(this.pledgeID, 'posted content');
    this.getPledgeDetails(this.pledgeID);
  }

  getPledgeDetails(pledeID) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    let memberID = window.localStorage.getItem("Member_ID");
    

    this.modelService.pledge_details(pledeID, memberID).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          this.pledgeDetails = data.details;
          //console.log(data.details, 'pledge');
          //console.log(data.details.pledge, 'my pledge');
          this.members_pledges_id = data.details.members_pledges_id;
          this.myPledge = data.pledge;
          if (this.myPledge != null) {
            this.viewMyPledge = true;
            this.pledged_amount = this.myPledge.pledged_amount;
          }
          
        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_001'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_002'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      });

    /*
    let post = {};
    post['pledge_id'] = pledeID;
    if(this.auth){
      post['member_id'] = window.localStorage.getItem("Member_ID");
    }

    this.modelService.pledge_details(post).subscribe(data => {
      if (data) {
        if (data.response == "Success") {
          this.pledgeDetails = data.details;
          //console.log(data.details, 'pledge');
          //console.log(data.details.pledge, 'my pledge');
          this.myPledge = data.pledge;
          if(this.myPledge != null){
            this.viewMyPledge = true;
            this.pledged_amount = this.myPledge.pledged_amount;
          }
          loading.dismiss();
        } else {

        }
      } else {

      }
    },
      error => {

      });
      */
  }


  pushLogin() {
    this.navCtrl.push(LoginPage, {
      return: 'PledgeDetailsPage',
      pledge_id: this.pledgeID
    });
  }

  pushRegister() {
    this.navCtrl.push(RegisterPage, {
      return: 'PledgeDetailsPage',
      pledge_id: this.pledgeID
    });
  }

  pushMyPledgeDetails() {
    this.navCtrl.push(MyPledgeDetailsPage, {
      members_pledges_id: this.members_pledges_id
    });
  }


  make_pledge(data: any) {
    //let pustData = {};
    data['member_id'] = window.localStorage.getItem("Member_ID");
    data['pledge_id'] = this.pledgeID;
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    this.modelService.make_pledge(data['member_id'], data['pledge_id'], data['amount']).subscribe(data => {
      loading.dismiss();
      if (data) {
        console.log(data);
        if (data.response == "Success") {
          this.navCtrl.push(MyPledgeDetailsPage, {
            members_pledges_id: this.members_pledges_id
          });
        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_001'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_002'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      });



  }



}
