import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelService } from '../../providers/model.service';

/**
 * Generated class for the EditAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-account',
  templateUrl: 'edit-account.html',
})
export class EditAccountPage {
  accountForm: FormGroup;
  name: any;
  phone: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public fb: FormBuilder,
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    private elementRef: ElementRef,
    public appCtrl: App,
    public events: Events
  ) {
    this.accountForm = fb.group({
      "phone": [null, Validators.required],
      "name": [null, Validators.required]
    });
  }

  ionViewDidLoad() {
    this.name = window.localStorage.getItem("Name");
    this.phone = window.localStorage.getItem("Phone");
  }


  editAccount(data: any) {
    //let pushData = {};
    data['member_id'] = window.localStorage.getItem("Member_ID");
    console.log(data);

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.edit_account(data).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          /* Store user data */
          this.modelService.loginSet(data.details);
          this.events.publish('user:login', data.details);
          this.modelService.showToast("Account successfully modified!");

        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: data.details.Msg + "!\nCode:&nbsp;" +  data.details.Code
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: "An error occurred, please retry!\nCode: ERR_001"
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: "An error occurred, please retry!\nCode: ERR_002"
        });
      });
  }

}
