import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPledgeDetailsPage } from './my-pledge-details';

@NgModule({
  declarations: [
    MyPledgeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyPledgeDetailsPage),
  ],
})
export class MyPledgeDetailsPageModule {}
