import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
declare var PaystackPop: any;

/**
 * Generated class for the MyPledgeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-pledge-details',
  templateUrl: 'my-pledge-details.html',
})
export class MyPledgeDetailsPage {

  email: string;
  ios: boolean;
  nameInterval: number;
  members_pledges_id: any;
  subAccount: any;
  transactionStatusMsg: any;
  date: any;
  total: any;
  postAmount: any;
  commission: any;
  paystackKey: any;
  ref: any;
  pledgeForm: FormGroup;
  amount_pending: number;
  amount_paid: any;
  pledged_amount: any;
  member_id: string;
  pledge: any;
  pledgeID: any;
  amount: any;

  viewDetails: boolean = true;
  viewPledge: boolean = false;
  viewSummary: boolean = false;
  summaryDone: boolean = false;
  showError: boolean;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    public fb: FormBuilder,
    public navParams: NavParams,
    private elementRef: ElementRef,
    private platform: Platform,
    private inAppBrowser: InAppBrowser) {
    this.member_id = window.localStorage.getItem("Member_ID");
    //this.pledgeID = navParams.get('pledge_id');
    this.members_pledges_id = navParams.get('members_pledges_id');

    this.pledgeForm = fb.group({
      amount: [null, Validators.required]
    });
  }

  ngOnInit() {
    if (this.platform.is('ios')) {
      this.ios = true;
    }

    this.email = window.localStorage.getItem("Email");
    this.member_id = window.localStorage.getItem("Member_ID");
  }

  ionViewDidLoad() {
    this.get_pledge_details(this.members_pledges_id);

    var ps = document.createElement("script");
    ps.type = "text/javascript";
    ps.src = "https://js.paystack.co/v1/inline.js";
    this.elementRef.nativeElement.appendChild(ps);



  }


  get_pledge_details(members_pledges_id) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.modelService.my_pledge_details(members_pledges_id).subscribe(data => {
      if (data) {
        if (data.response == "Success") {
          this.pledge = data.details;
          this.pledgeID = data.details.pledge_id;
          this.pledged_amount = this.pledge.pledged_amount;
          this.amount_paid = this.pledge.amount_paid == null ? 0.00 : this.pledge.amount_paid;
          let pending: number = parseFloat(this.pledged_amount) - parseFloat(this.amount_paid);
          this.amount_pending = pending <= 0 ? 0 : pending;

          console.log(data.details, 'details ple');

          loading.dismiss();
        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_001'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_002'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      });
  }


  summary(post: any) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();







    this.modelService.pledge_summary(this.member_id, post['amount']).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          this.ref = data.details.Ref;
          this.paystackKey = data.details.Paystack_Key;
          this.subAccount = data.details.Sub_Account;
          this.commission = data.details.Commission;
          this.postAmount = data.details.Post_Amount;
          this.total = data.details.Total;

          this._viewSummary()

        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_001'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_002'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      });

    /**/
  }

  //var url = Api_Url + "/pledge-done/" + member_id + "/" + pledge_id + "/" + members_pledges_id + "/"
  // + amount + "/" + commission + "/" + reference;

  payStack() {
    if (this.ios === true) {
      this.modelService.showToast("Redirecting to payment page, please wait...");

      const options: InAppBrowserOptions = {
        location: 'no',
        zoom: 'yes'
      }

      let url = this.modelService.SiteUrl + '/make-payment?type=pledge&key=' + this.paystackKey + '&email=' + this.email +
        '&amount=' + this.amount + '&ref=' + this.ref + '&member_id=' + this.member_id + '&commission=' + this.commission
        + '&pledge_id=' + this.pledgeID + '&members_pledges_id=' + this.members_pledges_id;

      console.log(url);
      const browser = this.inAppBrowser.create(url, '_system', options);


      browser.on("loadstop").subscribe(event => {
        browser.executeScript({ code: "localStorage.setItem('close', '')" });
        this.nameInterval = setInterval(function () {

          browser.executeScript({ code: "localStorage.getItem('close')" })
            .then((values) => {
              if (values[0] == 'true') {
                browser.close;
              }
            });

        }, 100)
      });


      browser.on("exit").subscribe(() => {
        clearInterval(this.nameInterval);
      });

    } else {
      this.modelService.showToast("Loading payment window, please wait...");
      try {
        let ref = this.ref
        var handler = PaystackPop.setup({
          key: this.paystackKey,
          email: window.localStorage.getItem("Email"),
          amount: this.postAmount,
          ref: ref,
          //subaccount: this.subAccount,
          // meta:[{ flightid:3849 }],
          callback: (response) => {
            // console.log('success. transaction ref is ' + response.reference);
            this.completeTransaction(response.reference);
          },
          onClose: function () {
            console.log('window closed');
          }
        });
        handler.openIframe();
      } catch (e) {
        // No content response..
        console.log(e);
        this.modelService.dfAlert({
          title: "Error",
          subTitle: 'Can not process your request, please retry!\nERR_000'
        });
      }
    }
  }


  completeTransaction(ref) {
    let data = {};
    data['pledge_id'] = this.pledgeID;
    data['ref'] = ref;
    data['amount'] = this.amount;
    data['commission'] = this.commission;
    console.log(data);
    /**/
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    // member_id, pledge_id, amount, commission, ref
    this.modelService.pledge_done(this.member_id, data['pledge_id'], this.members_pledges_id,
      data['amount'], data['commission'], ref).subscribe(data => {
        loading.dismiss();
        if (data) {
          if (data.response == "Success") {
            console.log(data.details);
            this.ref = ref;
            this.commission = this.commission;
            this.total = data.details.Total;
            this.date = data.details.Date;
            this.transactionStatusMsg = data.details.Status_Msg;
            this.summaryDone = true;

          } else {
            this.modelService.dfAlert({
              title: "Error",
              subTitle: 'Can not process your request, please retry!\nERR_001'
            });
            this._errorView();
          }
        } else {
          this.modelService.dfAlert({
            title: "Error",
            subTitle: 'Can not process your request, please retry!\nERR_002'
          });
          this._errorView();
        }
      },
        error => {
          loading.dismiss();
          this._errorView();
        });
  }


  _errorView() {
    this.viewDetails = false;
    this.viewPledge = false;
    this.viewSummary = false;
    this.summaryDone = false;
    this.showError = true;
  }

  _reset() {
    this.viewDetails = true;
    this.viewPledge = false;
    this.viewSummary = false;
    this.summaryDone = false;
    this.showError = false;

    this.transactionStatusMsg = '';
    this.date = '';
    this.total = '';
    this.postAmount = '';
    this.commission = '';
    this.paystackKey = '';
    this.subAccount = '';
    this.ref = '';
    this.amount = '';
  }

  _viewDetails() {
    this.viewDetails = true;
    this.viewPledge = false;
    this.viewSummary = false;
  }

  _viewPledge() {
    this.viewDetails = false;
    this.viewPledge = true;
    this.viewSummary = false;
  }

  _viewSummary() {
    this.viewDetails = false;
    this.viewPledge = false;
    this.viewSummary = true;
  }

}
