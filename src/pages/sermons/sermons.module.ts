import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SermonsPage } from './sermons';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SermonsPage,
  ],
  imports: [
    IonicPageModule.forChild(SermonsPage),
    PipesModule
  ],
})
export class SermonsPageModule {}
