import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';

import { Http } from '@angular/http';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import { YtProvider } from './../../providers/yt/yt';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the SermonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sermons',
  templateUrl: 'sermons.html',
  // providers:[YoutubeServiceProvider]
})

export class SermonsPage {
  error = {};
  show_content: boolean;
  channelId = 'PLUMP1EBixvvwRm0XZkV8gMR6yWJOY7yfW';
  playlists: Observable<any[]>;
  videos: Observable<any[]>;
  
  constructor(
    public navCtrl: NavController,
    public http: Http, 
    public nav:NavController, 
    private elementRef: ElementRef,
    // public ytPlayer: YoutubeServiceProvider,
    public loadingCtrl: LoadingController,
    private youtube: YoutubeVideoPlayer, 
    private ytProvider: YtProvider, 
    private alertCtrl: AlertController,
    private plt: Platform) {
  }

  ngOnInit() {
    
  }


  ionViewDidLoad() {
    // PLUMP1EBixvvwRm0XZkV8gMR6yWJOY7yfW
    this.getVideos(this.channelId);
  }


  getVideos(listId) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    this.videos = this.ytProvider.getListVideos(listId);
    this.videos.subscribe(data => {
      loading.dismiss();
      if(!data){
        this.show_content = false;
        this.error = { text: 'Can not load data, please retry!', code: 'ERR_001' }
        return;
      }
      this.show_content = true;
      // console.log('playlists: ', data);
    }, err => {
      loading.dismiss();
      this.show_content = false;
      this.error = { text: 'Can not load data, please retry!', code: 'ERR_000' }
    })
  }

  openVideo(video) {
    if (this.plt.is('cordova')) {
      this.youtube.openVideo(video.snippet.resourceId.videoId);
    } else {
      window.open('https://www.youtube.com/watch?v=' + video.snippet.resourceId.videoId);
    }
  }

  


}
