import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { Observable } from 'rxjs/Observable';
import { GalleryDetailsPage } from '../gallery-details/gallery-details';

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {
  galleries: Observable<any[]>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modelService: ModelService,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.getGallery();
  }


  getGallery(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.gallery().subscribe(data => {
      loading.dismiss();
      if (data) { 
        //console.log(data);
        this.galleries = data.reverse();
        /*
        if (data.response == "Success") {
          this.galleries = data.details;
          console.log(data.details);
        } else {

        }
        */
      } else {

      }
    },
      error => {
        loading.dismiss();
      });
  }


  

  openAlbum(gallery){ // console.log(gallery);
    this.navCtrl.push(GalleryDetailsPage, {
      id: gallery.Id,
      title: gallery.Title
    });
  }



}
