import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutServicesPage } from './about-services';

@NgModule({
  declarations: [
    AboutServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutServicesPage),
  ],
})
export class AboutServicesPageModule {}
