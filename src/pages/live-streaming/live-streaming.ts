import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

/**
 * Generated class for the LiveStreamingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-live-streaming',
  templateUrl: 'live-streaming.html',
})
export class LiveStreamingPage {

  video_url:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    public youtube: YoutubeVideoPlayer) {
  }

  //https://www.youtube.com/embed/live_stream?channel=UCo6uyrnKYeeMZssK8fAlW8Q
  //http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_20mb.mp4

  ionViewDidLoad() {

    /*
    this.videoPlayer.play('http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_20mb.mp4').then(() => {
      console.log('video completed');
      alert('video shown');
     }).catch(err => {
      console.log(err);
      alert('video error');
     });
     */

     //setTimeout(() => {
      this.video_url = 'https://www.youtube.com/embed/live_stream?channel=UCo6uyrnKYeeMZssK8fAlW8Q';
      this.video_url = this.sanitizer.bypassSecurityTrustResourceUrl(this.video_url);
     //}, 500);

    




  }

}
