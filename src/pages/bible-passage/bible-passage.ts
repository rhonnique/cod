import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';

/**
 * Generated class for the BiblePassagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bible-passage',
  templateUrl: 'bible-passage.html',
})
export class BiblePassagePage {

  passage: any;
  error = {};
  show_content: boolean;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "Daily Bible Passage";
  /** Toggle Toolbar Ends */

  constructor(
    public navCtrl: NavController,
    public modelService: ModelService,
    private elementRef: ElementRef,
    public loadingCtrl: LoadingController) {
  }

  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }

  ionViewDidLoad() {
    this.daily_passage();
  }



  daily_passage() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.daily_passage().subscribe(data => {
      loading.dismiss();
      if (data.response === true && data.details) {
        this.show_content = true;
        this.passage = data.details.details;
      } else {
        this.error = { text: 'Can not load passage, please retry!', code: 'ERR_001' }
      }
    },
      error => {
        loading.dismiss();
        this.error = { text: 'Can not load passage, please retry!', code: 'ERR_000' }
      });
  }

}
