import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BiblePassagePage } from './bible-passage';

@NgModule({
  declarations: [
    BiblePassagePage,
  ],
  imports: [
    IonicPageModule.forChild(BiblePassagePage),
  ],
})
export class BiblePassagePageModule {}
