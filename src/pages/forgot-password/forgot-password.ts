import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { LoginPage } from '../login/login';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  forgotForm: FormGroup;
  pushLogin: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public loadingCtrl: LoadingController,
    public modelService: ModelService) {
    this.pushLogin = LoginPage;

    this.forgotForm = fb.group({
      email: [null, Validators.required]
    });
  }

  ionViewDidLoad() {

  }

  submit(request: any) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    this.modelService.forgot_password(request).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (typeof data.Success != 'undefined' && data.Success === false) {
          this.modelService.dfAlert({
            title: "Reset Error",
            subTitle: data.Message
          });
        } else {
          /*** REQUEST SUCESSFUL ***/
          //this.modelService.loginSet(data.Data);
          //this.events.publish('user:login', data.Data);
          //this.navCtrl.setRoot(this.push, pustData);
          this.fb.group({
            email: ['']
          });

          this.modelService.showToast("Password successfully reset!");
        }
      } else {
        this.modelService.dfAlert({
          title: "Reset Error",
          subTitle: "Can not complete your request, please retry!\nCode: ERR_001"
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Reset Error",
          subTitle: "Can not complete your request, please retry!\nCode: ERR_000"
        });
      });
  }



}
