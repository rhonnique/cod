import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelService } from '../../providers/model.service';
import { HomePage } from '../home/home';
import { DonationsPage } from '../donations/donations';
import { PledgeDetailsPage } from '../pledge-details/pledge-details';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  pushLogin: any;
  pledge_id: any;
  donation_type_others: any;
  donation_type: any;
  return: any;
  donation_amount: any;
  push: any;
  registerForm: FormGroup;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    private elementRef: ElementRef,
    public appCtrl: App,
    public events: Events) {

      this.pushLogin = LoginPage;

    this.return = navParams.get('return');
    this.donation_amount = navParams.get('donation_amount');
    this.donation_type = navParams.get('donation_type');
    this.donation_type_others = navParams.get('donation_type_others');

    this.pledge_id = navParams.get('pledge_id');

    this.registerForm = fb.group({
      "username": [null, Validators.required],
      "password": [null, Validators.required],
      "password_confirm": [null, Validators.required],
      "first_name": [null, Validators.required],
      "last_name": [null, Validators.required],
      //"email": [null, Validators.required],
      'email': [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

  ionViewDidLoad() {
    //'pin': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{4}$")])]
  }


  register(data: any) {

    let pustData = {};

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    this.modelService.register(data['email'],data['username'],data['password'],
    data['first_name'],data['last_name']).subscribe(data => {
      loading.dismiss();
      if (data) {
        // console.log(data.Success, 'Success');
        if (typeof data.Success != 'undefined' && data.Success === false) {
          this.modelService.dfAlert({
            title: "Registration Error",
            subTitle: data.Message
          });
        } else {
          this.modelService.loginSet(data.Data);

          switch (this.return) {
            case 'DonationsPage':
              pustData['donation_amount'] = this.donation_amount;
              pustData['donation_type'] = this.donation_type;
              pustData['donation_type_others'] = this.donation_type_others;
              this.push = DonationsPage;
              break;

            case 'PledgeDetailsPage':
              this.push = PledgeDetailsPage;
              pustData['pledge_id'] = this.pledge_id;
              break;

            default:
              this.push = HomePage;
          }

          this.events.publish('user:login', data.Data);
          this.modelService.showToast("Account successfully created!");
          this.navCtrl.setRoot(this.push, pustData);

        }
      } else {
        this.modelService.dfAlert({
          title: "Registration Error",
          subTitle: "Incomplete registration, please retry!\nCode: ERR_001"
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Registration Error",
          subTitle: "Incomplete registration, please retry!\nCode: ERR_000"
        });
      });
  }

}
