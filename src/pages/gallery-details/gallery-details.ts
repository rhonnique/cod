import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';

/**
 * Generated class for the GalleryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gallery-details',
  templateUrl: 'gallery-details.html',
})
export class GalleryDetailsPage {
  ID: any;
  title: string;
  photos: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modelService: ModelService) {
    this.ID = this.navParams.get('id');
    this.title = this.navParams.get('title');
  }

  ionViewDidLoad() {
    this.getGalleryDetails(this.ID);
  }

  getGalleryDetails(id){
    this.modelService.gallery_details(id).subscribe(data => {
      if (data) {
        console.log(data);
        this.photos = data;
      } else {

      }
    },
      error => {

      });
  }

}
